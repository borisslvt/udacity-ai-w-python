# Euclidean algorithm to determine Great Common Divisor - Domenico Boris Salvati

def GCD(a,b):
    '''
    ## Euclidean Algorithm to determine Great Common Divisor    ##
    ## by Domenico Boris Salvati                                ##

    Algorithm that finds the Greatest Common Divisor between two natural numbers.
    Given two natural numbers, 'a' and 'b', the algorithm will recursiverly update the
    value of 'a' and 'b' with the new modulo of the division, until this is equal to 0.

    The last value for which is possible to get a modulo != 0, is the GCD.
    '''
    # add a timer

    # Init the steps counter
    step = 0
    # Init local variables with global value (input by user)
    alfa, beta = a, b
    while beta != 0:
        # Increase counter
        step += 1
        print('Step : ' + str(step) + '\n computing : {} / {} \n modulo = {}'.format(alfa,beta,alfa%beta))
        # Update local variables
        alfa, beta = beta, alfa % beta
    # When modulo converges to zero
    print('the Greatest Commond Divider between {} and {} is : {}'.format(a, b, alfa))
    print('it took {} step(s) to calculate GCD.'.format(step))

# Calling the algorithm

# Algorithm On
print(GCD.__doc__)
print('\n Initializing..')
run = 'y'

while run == 'y':
    try:
        a = int(input('Enter Numerator :'))
        b = int(input('Enter Divisor : '))
    except ValueError:
        print('\nNumerator/Divisor must be numbers. Enter a valid number')
    else:
        GCD(a,b)
    finally:
        run = input("\n Do you want to run the Algorithm again? (y or n) ")
